package Ex2_SecondBiggestNumber;

import java.util.List;

public interface SecondBiggestNumber {
    int getSecondBiggestNumber(List<Integer> array);
}

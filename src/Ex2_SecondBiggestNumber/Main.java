package Ex2_SecondBiggestNumber;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        int arraySize;
        Scanner sc=new Scanner(System.in);
        SecondBiggestNumber secondBiggestNumber = (arrayList) -> {
            Collections.sort(arrayList,(i1,i2) -> {
                if(i1<i2)
                    return 1;
                else if (i1>i2) {
                    return -1;
                }
                else return 0;
            });
            return arrayList.get(1);
        };

        System.out.print("Enter the number of elements in the list: ");
        arraySize=sc.nextInt();
        List<Integer> array = new ArrayList<>();
        System.out.println("Enter the elements: ");
        for(int i=0; i<arraySize; i++)
        {
            array.add(sc.nextInt());
        }
        System.out.println("The second biggest number entered is : "+ secondBiggestNumber.getSecondBiggestNumber(array));
    }

}

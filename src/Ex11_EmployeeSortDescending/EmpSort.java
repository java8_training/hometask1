package Ex11_EmployeeSortDescending;

import java.util.List;

public interface EmpSort {
    void empSort(List<Employee> employeeList);
}

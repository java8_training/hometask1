package Ex6_EmployeeSort;

import java.util.List;

public interface EmpSort {
    void empSort(List<Employee> employeeList);
}

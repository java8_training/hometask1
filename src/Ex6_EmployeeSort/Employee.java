package Ex6_EmployeeSort;

public class Employee {
    int empNo;
    String empName;

    Employee(int eNo, String eName){
        empNo = eNo;
        empName = eName;
    }

    public String toString(){
        return "Emp id : "+empNo+"\nEmp Name : "+empName;
    }

}

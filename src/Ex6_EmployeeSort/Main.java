package Ex6_EmployeeSort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<Employee> empList = new ArrayList<>();
        int empNo,numberOfEmployees;
        String empName;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        EmpSort empSort = (employeeList) -> {
            Collections.sort(employeeList, (employee1, employee2)-> {
                if (employee1.empName.compareTo(employee2.empName)>0)
                    return 1;
                else if (employee1.empName.compareTo(employee2.empName)<0)
                    return -1;
                else return 0;
            });
            System.out.println("After Sorting : ");
            for (Employee e:employeeList) {
                System.out.println(e.toString());
            }
        };

        System.out.print("Enter the number of employees : ");
        numberOfEmployees= Integer.parseInt(reader.readLine());

        for(int i=0; i<numberOfEmployees; i++)
        {
            System.out.print("Enter the emp id : ");
            empNo = Integer.parseInt(reader.readLine());
            System.out.print("Enter the emp name : ");
            empName = reader.readLine();
            empList.add(new Employee(empNo,empName));
        }

        System.out.println("Before Sorting : ");
        for (Employee e:empList) {
            System.out.println(e.toString());
        }

        empSort.empSort(empList);
    }
}

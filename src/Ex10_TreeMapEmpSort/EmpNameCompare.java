package Ex10_TreeMapEmpSort;

import java.util.Comparator;

public class EmpNameCompare implements Comparator<Employee> {
    public int compare(Employee e1, Employee e2)
    {
        return e2.empName.compareTo(e1.empName);
    }
}

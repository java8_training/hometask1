package Ex10_TreeMapEmpSort;

public class Employee {
    String empNo;
    String empName;

    Employee(String eNo, String eName){
        empNo = eNo;
        empName = eName;
    }

    public String toString(){
        return "Emp id : "+empNo+" Emp Name : "+empName+"\n";
    }

}

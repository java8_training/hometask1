package Ex10_TreeMapEmpSort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) throws IOException {
        Map<Employee, Object> employeeMap = new TreeMap<>(new EmpNameCompare());
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String empNo = "", empName = "";

        System.out.print("Enter the number of employees in the treemap: ");
        int treeMapSize = Integer.parseInt(reader.readLine());
        System.out.println("Enter the emp details: ");
        for (int i = 0; i < treeMapSize; i++) {
            System.out.print("Enter the emp id : ");
            empNo = reader.readLine();
            System.out.print("Enter the emp name : ");
            empName = reader.readLine();
            employeeMap.put(new Employee(empNo, empName), i);
        }
        System.out.println("Employee TreeMap\n"+employeeMap);
    }
}

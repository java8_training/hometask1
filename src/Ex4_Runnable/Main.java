package Ex4_Runnable;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter starting number : ");
        int startingNumber=sc.nextInt();

        System.out.print("Till which number to print : ");
        int endNumber=sc.nextInt();
        Thread thread = new Thread(() -> {
            if(startingNumber>endNumber)
                System.out.println("incorrect range");
            else {
                for (int i = startingNumber; i <= endNumber; i++)
                    System.out.println(i);
            }
        });
        thread.start();
    }
}

package Ex8_TreeSetEmployeeSort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) throws IOException {
        TreeSet<Employee> empTreeSet = new TreeSet<Employee>(new EmpNameCompare());
        int empNo,numberOfEmployees;
        String empName;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter the number of employees : ");
        numberOfEmployees= Integer.parseInt(reader.readLine());

        for(int i=0; i<numberOfEmployees; i++)
        {
            System.out.print("Enter the emp id : ");
            empNo = Integer.parseInt(reader.readLine());
            System.out.print("Enter the emp name : ");
            empName = reader.readLine();
            empTreeSet.add(new Employee(empNo,empName));
        }

        for (Employee e:empTreeSet) {
            System.out.println(e.toString());
        }

    }
}

package Ex9_TreeMapSort;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Map<String, String> treemap = new TreeMap<>(Collections.reverseOrder());
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String key ="",value="";
        System.out.print("Enter the number of elements in the treemap: ");
        int treeSetSize= Integer.parseInt(reader.readLine());
        System.out.println("Enter the elements: ");
        for(int i=0; i<treeSetSize; i++)
        {
            System.out.print("Enter the key : ");
            key = reader.readLine();
            System.out.print("Enter the value : ");
            value = reader.readLine();
            treemap.put(key,value);
        }
        System.out.println("TreeMap\n" + treemap);
    }
}

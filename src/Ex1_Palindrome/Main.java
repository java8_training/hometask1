package Ex1_Palindrome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String wordEntered = "";
        Palindrome palindrome = (check) -> {
            String revString = "";
            for(int i = check.length() - 1; i >= 0; i--) {
                revString = revString + check.charAt(i);
            }
            if(check.equalsIgnoreCase(revString))
                return "The string is palindrome.";
            else
                return "The string is not palindrome.";
        };

        do{
            System.out.println("Enter a word to check if it is a palindrome / 0 to terminate : ");
            wordEntered = reader.readLine();
            if(!wordEntered.equals("0"))
                System.out.println(palindrome.checkPalindrome(wordEntered));
        }while (!wordEntered.equals("0"));
    }
}

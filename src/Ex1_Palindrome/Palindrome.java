package Ex1_Palindrome;

@FunctionalInterface
public interface Palindrome {
    String checkPalindrome(String checkString);

}

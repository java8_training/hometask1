package Ex5_SortReverseOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int arraySize;
        Scanner sc=new Scanner(System.in);
        SortReverseOrder sortReverseOrder = (arrayList) -> {
            Collections.sort(arrayList,(i1,i2) -> {
                if(i1<i2)
                    return 1;
                else if (i1>i2) {
                    return -1;
                }
                else return 0;
            });
            System.out.println(arrayList);;
        };

        System.out.print("Enter the number of elements in the list: ");
        arraySize=sc.nextInt();
        List<Integer> array = new ArrayList<>();
        System.out.println("Enter the elements: ");
        for(int i=0; i<arraySize; i++)
        {
            array.add(sc.nextInt());
        }
        sortReverseOrder.sortReverseOrder(array);
    }

}

package Ex5_SortReverseOrder;

import java.util.List;

public interface SortReverseOrder {
    void sortReverseOrder(List<Integer> array);
}

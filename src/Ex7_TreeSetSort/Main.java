package Ex7_TreeSetSort;

import java.util.Scanner;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<Integer> integerTreeSet = new TreeSet<>();
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter the number of elements in the treeset: ");
        int treeSetSize=sc.nextInt();
        System.out.println("Enter the elements: ");
        for(int i=0; i<treeSetSize; i++)
        {
            integerTreeSet.add(sc.nextInt());
        }
        System.out.println("TreeSet\n" + integerTreeSet);
        TreeSet<Integer> sortedTreeSet = (TreeSet<Integer>) integerTreeSet.descendingSet();
        System.out.println("TreeSet after sorting in descending order\n" + sortedTreeSet);
    }
}

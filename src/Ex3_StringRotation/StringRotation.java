package Ex3_StringRotation;

public interface StringRotation {
    boolean checkStringRotation(String s1,String s2);
}

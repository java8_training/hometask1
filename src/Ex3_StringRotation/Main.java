package Ex3_StringRotation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String stringOne,stringTwo;
        StringRotation sr = (s1,s2) -> {
            if(s1 == null || s2 == null || s1.length() != s2.length())
                return false;
            else {
                String combinedString = s1 + s1;
                return combinedString.contains(s2);
            }
        };

        System.out.println("Enter first string : ");
        stringOne = reader.readLine();
        System.out.println("Enter second string : ");
        stringTwo = reader.readLine();
        if(sr.checkStringRotation(stringOne,stringTwo))
            System.out.println(stringTwo+" is a rotation of "+stringOne);
        else System.out.println(stringTwo+" is not a rotation of "+stringOne);
    }
}
